FROM busybox
MAINTAINER Chedi Toueiti <chedi.toueiti@gmail.com>

VOLUME /var/lib/pgsql/

CMD ["/bin/sh"]